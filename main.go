package main

import (
	"fmt"

	"github.com/kataras/iris"
)

func main() {
	app := iris.Default()

	app.Get("/someGet/:wew", getting)

	app.Get("/user", userDetail)

	app.Post("/user", throw)

	app.Run(iris.Addr(":9000"))
}

func getting(c iris.Context) {
	path := c.Params().Get("wew")
	c.Writef("kamu siapa?\naku %s", path)
}

func userDetail(c iris.Context) {
	param := c.URLParams()["nama"]
	c.Writef("namaku %s", param)
}

func throw(c iris.Context) {
	type Req struct {
		Name string `json:"name"`
		Age  int    `json:"age"`
	}

	user := &Req{}
	err := c.ReadJSON(user)
	if err != nil {
		fmt.Printf("error: %s\n", err.Error())
		return
	}

	fmt.Printf("form: %s", user)
	c.StatusCode(iris.StatusOK)
	c.JSON(user)
}
